package junia.lab04.web.controller;

import junia.lab04.core.entity.Company;
import junia.lab04.core.service.CompanyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;



@Controller
public class CompanyController {
    private final static Logger LOGGER = LoggerFactory.getLogger(CompanyController.class);

    CompanyService companyDAO;

    public CompanyController(CompanyService companyDAO) {
        this.companyDAO=companyDAO;
    }

    @RequestMapping(value="/list",method = RequestMethod.GET)
    public String getListOfCompanies(ModelMap map) {
        LOGGER.info("Getting compnaiesList from CompanyService");
        List<Company> companiesList= companyDAO.findAllWithProjects();
        LOGGER.info("adding attribute compnaiesList : {}", companiesList);
        map.addAttribute("companies", companiesList);
        return "companiesList";
    }

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public String getForm(ModelMap modelMap){
        LOGGER.info("Getting company form");
        Company company = new Company();
        modelMap.addAttribute("company", company);

        return "companyForm";
    }

    @RequestMapping(value = "/form", method = RequestMethod.POST)
    public String submitForm(@ModelAttribute("company") Company company){
        LOGGER.info("Posting company form");
        LOGGER.info("Company : {}", company);
        companyDAO.save(company);
        return "redirect:/list";

    }

    @RequestMapping(value = "/{id}/delete", method = RequestMethod.GET)
    public String deleteCompany(@PathVariable("id") long id){
        LOGGER.info("Deleting company");
        LOGGER.info("Company ID : {}", id);
        companyDAO.deleteById(id);
        return "redirect:/list";

    }


}
