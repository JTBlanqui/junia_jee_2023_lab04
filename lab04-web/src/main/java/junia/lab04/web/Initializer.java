package junia.lab04.web;

import junia.lab04.core.config.AppConfig;
import junia.lab04.core.config.DBConfig;
import junia.lab04.web.config.WebConfig;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class Initializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        Class[] configs = new Class[2];

        configs[0] = AppConfig.class;
        configs[1] = DBConfig.class;

        return configs;
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        Class[] config = new Class[1];

        config[0] = WebConfig.class;

        return config;
    }

    @Override
    protected String[] getServletMappings() {
        String[] mapping = new String[1];

        mapping[0] = "/";

        return mapping;
    }
}

